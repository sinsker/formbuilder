var formBuilderUt = (function ($) {

    "use strict";

    var ROLE_ERROR = 'ROLE_ERROR',
        UNDEFINED_FORM_BUILDER_ERROR = 'UNDEFINED_FORM_BUILDER_ERROR',
        EMPTY_ROOT_DOM_ERROR = 'EMPTY_ROOT_DOM_ERROR',
        WRONG_CUSTOM_ROLE_TYPE_ERROR = 'WRONG_CUSTOM_ROLE_TYPE_ERROR';

    var INPUT_TYPE = {
        INPUT: 'INPUT',
        CHECKBOX_RADIO: 'CHECKBOX_RADIO',
        CHECKBOX_RADIO_MODULE: 'CHECKBOX_RADIO_MODULE',
        SELECT: 'SELECT',
        SELECT_MODULE: 'SELECT_MODULE'
    };

    var REX_ROLE = {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        url: /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/,
        lowerCase: /^[a-z]+$/,
        upperCase: /^[A-Z]+$/,
        en: /^[A-Za-z]+$/
    }

    var inputKeyValueAttrGroup = ['text','hidden','file'];
    var inputTextarea = 'textarea';
    var inputCheckAttrGroup = ['checkbox','radio'];
    var inputSelectTagGroup = ['select'];

    var builderRoleHtmlAttr = 'builder-role';

    var validation = {};


    (function(){
        var defaultFunctions = {
            min: function (param, optionArray) {
                var number = optionArray[0];
                if (_getStringLength(param) < number) {
                    return true;
                }
            },
            max: function (param, optionArray) {
                var number = optionArray[0];
                if (_getStringLength(param) > number) {
                    return true;
                }
            },
            dataType: function (param, optionArray) {
            },
            require: function (param) {
                if (_isEmpty(param) || _isEmptyString(param)) {
                    return true;
                }
            },
            noncheck: function (param) {
                return defaultFunctions.require(param);
            },
            nonselect: function (param) {
                return defaultFunctions.require(param);
            },
            extfile: function(param, optionsArray){
                var optionExtList = optionsArray[0].split(',')
                var ext = _getExtensionOfFilename(param);
                if(optionExtList.indexOf(ext) === -1){
                    return true;
                }
            },
            url: function (param) {
                return !REX_ROLE.url.test(param)
            },
            lower: function(param) {
                return !REX_ROLE.lowerCase.test(param)
            },
            upper: function(param) {
                return !REX_ROLE.upperCase.test(param)
            },
            en: function(param) {
                return !REX_ROLE.en.test(param)
            },
            email:function(param) {
                return !REX_ROLE.email.test(param)
            }
        };
        validation['function'] = defaultFunctions;
        validation['message'] = {
            min: '$1 값은 최소 $2 보다 커야합니다.',
            max: '$1 값의 $2 초과하였습니다.',
            dataType: '$1 값은 $2 타입이 아닙니다.',
            require: '$1 값은 필수값입니다.',
            noncheck: '$1 하나 이상 체크해야합니다.',
            nonselect: '$1 를 선택해야합니다.',
            extfile: '$1 타입이 올바르지 않습니다.',
            url: '$1 이 URL 형태가 아닙니다.',
            lower: '$1 는 소문자이여야 합니다.',
            upper: '$1 는 대문자이여야 합니다.',
            en: '$1 는 영어로만 구성되어야합니다.',
            email: '$1 는 이메일 형태가 아닙니다.'
        }
    })();

    /*============================================================================================*/

    function _isArray(data){
        return data instanceof Array;
    }

    function _isObject(data){
        return data instanceof Object;
    }

    function _isEmpty(data){
        if(typeof data === 'undefined' || data === ''){
            return true;
        }
        if(_isArray(data)){
            return data.length === 0;
        }else if(_isObject(data)){
            return Object.keys(data).length === 0;
        }
        return false;
    }

    //공백만 있는지 체크
    function _isEmptyString(str) {
        return /^\s*$/ig.test(str);
    }

    // 문자열 길이 체크 알파뉴메릭(1자리), 한글(2자리)
    function _getStringLength (str){
        var strLength = 0;
        for (var i = 0; i < str.length; i++){
            var code = str.charCodeAt(i)
            var ch = str.substr(i,1).toUpperCase()
            code = parseInt(code);
            if ((ch < "0" || ch > "9") && (ch < "A" || ch > "Z") && ((code > 255) || (code < 0)))
                strLength = strLength + 2;
            else
                strLength = strLength + 1;
        }
        return strLength;
    }

    function _getExtensionOfFilename(str){
        var strLength = str.length;
        var lastDot = str.lastIndexOf('.');
        return str.substring(lastDot+1, strLength).toLocaleLowerCase();
    }

    //validation role 함수 존재여부
    function _isValidationFunction(roleFunction){
        return validation.function.hasOwnProperty(roleFunction)
    }

    //validation 발생
    function _isValidationEmit(paramValue, roleFunction, roleFunctionOption){
        return  validation.function[roleFunction](paramValue, roleFunctionOption);
    }

    /* Modal ====================================================================================================================================================*/


    // Validation 메시지
    function ValidationCheckFormatException(key, value, message, role, roleOptions){
        this.key = key;
        this.value = value;
        this.message = message;
        this.validation = {
            role: role,
            options: roleOptions
        }
    }

    /*======================================================================================================================================================*/

    function _templateReplace(template, bindingDatas){
        if(_isEmpty(template)) return template;

        for(var i in bindingDatas){
            var reg = new RegExp('\\$'+(+i+1), 'gi');
            template = template.replace(reg, bindingDatas[i]);
        }
        return template;
    }

    //파라미터 탐색시 기본 액션
    function _searchActionDefault(paramKey, paramValue){
        return paramValue;
    }

    //파라미터 탐색시 체크 액션
    function _searchActionValidate(paramKey, paramValue, formBuilder){
        var paramValidation = formBuilder.validation;
        if(_isEmpty(paramValidation)) return value;

        var roles = paramValidation.roles;
        var message = paramValidation.messages;

        if(roles.hasOwnProperty(paramKey)){
            var role = roles[paramKey];
            for(var i in role){
                var roleFunction = role[i];
                var roleFunctionOption = [];
                if(roleFunction.indexOf(':') !== -1){
                    var roleObject = roleFunction.split(':');
                    roleFunction = roleObject[0];
                    roleFunctionOption.push(roleObject[1]);
                }
                if( _isValidationFunction(roleFunction) && _isValidationEmit(paramValue, roleFunction, roleFunctionOption) ) {
                    var templateBindDatas = [paramKey, roleFunctionOption[0]]
                    var messageTemplate = _templateReplace(message[roleFunction], templateBindDatas);
                    throw new ValidationCheckFormatException(paramKey, paramValue, messageTemplate, roleFunction, roleFunctionOption);
                }
            }
        }
        return paramValue;
    }

    //파라미터 탐색시 html Update 액션
    function _searchActionHtmlUpdate(paramKey, paramValue, formBuilder){
        var $html = formBuilder.html;
        var $input = $html.find('input[name='+paramKey+'], textarea');
        var $select = $html.find('.select[name='+paramKey+']');
        if($input && $input.length > 0){
            $input.each(function(){
                var $this = $(this);
                htmlInputTypeFactory(paramKey, paramValue, $this);
            })
        }

        if($select && $select.length > 0){
            $select.each(function(){
                var $this = $(this);
                htmlInputTypeFactory(paramKey, paramValue, $this);
            })
        }

        return paramValue;
    }


    //기본 INPUT 처리
    function _isDefaultInput(paramKey, paramValue, $html){
        var inputType = $html.attr('type');

        if(inputKeyValueAttrGroup.indexOf(inputType) !== -1 || $html.prop('tagName') === 'textarea'){
            $html.val(paramValue);
        }
        return false;
    }

    //체크박스&라디오박스 처리
    function _isCheckboxOrRadio(paramKey, paramValue, $html){
        var inputType = $html.attr('type');
        if(inputCheckAttrGroup.indexOf(inputType) !== -1 && $html.closest(".radio").length === 0 && $html.closest(".checkbox").length === 0 ){
            $html.prop('checked', false);
            if(_isArray(paramValue)){
                for(var i in paramValue){
                    if($html.val() === paramValue[i]){
                        $html.prop('checked',true);
                        return true;
                    }
                }
            }else{
                if($html.val() === paramValue){
                    $html.prop('checked',true);
                    return true;
                }
            }
        }
    }

    //셀렉트 박스 처리
    function _isSelect(paramKey, paramValue, $html){
        if(inputSelectTagGroup.indexOf($html.get(0).tagName) !== -1){
            $html.find('option[value='+paramValue+']').prop("selected", true);
            return true;
        }
    }



    var htmlInputTypeFactory = function(paramKey, paramValue, $html){
        _isDefaultInput(paramKey, paramValue, $html) ?
            true : _isCheckboxOrRadio(paramKey, paramValue, $html) ?
            true : _isSelect(paramKey, paramValue, $html)
    };

    // 파라미터 탐색
    var _searchParams = function(formBuilder, action){
        if(Object.keys(formBuilder.params).length > 0){
            return _getRoofParams(formBuilder, action)
        }else{
            return null;
        }
    };

    //파라미터 재귀 호출
    var _getRoofParams = function(formBuilder, action){
        var params = formBuilder.params;
        var validation = formBuilder.validation;
        for(var key in params){
            if(params[key] instanceof _formBuilder){
                params[key] = _getRoofParams(params[key].all(), params[key].validation, formBuilder, action)
            }else if(_isObject(params[key]) && !_isArray(params[key]) ){
                params[key] = _getRoofParams(params[key],validation, formBuilder, action)
            }else{
                params[key] = action(key, params[key], formBuilder)
            }
        }
        return params;
    };

    /*====================================================================================================================================================*/

    //_formBuilder 생성자
    function _formBuilder(params){
        this.params = params || {}
        this.validation = {
            roles: {},
            messages: {}
        };
        return this;
    }

    _formBuilder.prototype.$html = function(html){
        this.html = html;
        return this;
    }

    //파라미터 가져오기
    _formBuilder.prototype.get = function(key){
        if(!this.params.hasOwnProperty(key)){
            return null;
        }
        return this.params[key];
    };

    // 파라미터 저장
    _formBuilder.prototype.set = function(key, value){
        if(key instanceof Object && !value){
            var object = key;
            $.extend({}, this.params, object)
        }else{
            this.params[key] = value;
        }
        return this;
    }

    _formBuilder.prototype.isEmpty = function(){
        return _isEmpty(this.params);
    };

    //파라미터 전체 불러오기
    _formBuilder.prototype.all = function(){
        return _searchParams(this, _searchActionDefault)
    };

    //HTML 업데이트
    _formBuilder.prototype.htmlUpdate = function($html){

        if($html) this.html = $html;
        if(_isEmpty(this.html)) throw new Error(EMPTY_ROOT_DOM_ERROR)

        return _searchParams(this, _searchActionHtmlUpdate)
    };

    //벨리데이션 불러오기
    _formBuilder.prototype.getValidation = function(){
        return this.validation;
    };

    //벨리데이션 저장
    _formBuilder.prototype.setValidation = function(validationOptions){
        if(!validationOptions || !validationOptions.hasOwnProperty('roles')){
            throw new Error(ROLE_ERROR)
        }
        this.validation.roles = validationOptions.roles;
        if(!validationOptions.hasOwnProperty('messages')){
            validationOptions['messages'] = {}
        }
        this.validation.messages = $.extend({}, validation.message, validationOptions.messages);
        if(validationOptions.hasOwnProperty('customRole')){
            this.setCustomValidationRule(validationOptions.customRole)
        }
        return this;
    };

    //커스텀 룰 적용
    _formBuilder.prototype.setCustomValidationRule = function(customRole){
        if(!_isObject(customRole)){
            throw new Error(WRONG_CUSTOM_ROLE_TYPE_ERROR)
        }
        for(var roleName in customRole){
            if(!customRole[roleName].hasOwnProperty('role')){
                throw new Error(WRONG_CUSTOM_ROLE_TYPE_ERROR)
            }
            validation.function[roleName] = customRole[roleName].role;

            if(customRole[roleName].hasOwnProperty('message')){
                this.validation.messages[roleName] = customRole[roleName].message;
            }
        }

    };


    //파라미터 validation 체크
    _formBuilder.prototype.validate = function(){
        try{
            _searchParams(this, _searchActionValidate)
        }catch(e){
            if(e instanceof ValidationCheckFormatException){
                return e;
            }else{
                console.log(e)
            }
        }
        return true;
    };

    //파라미터 비우기
    _formBuilder.prototype.clear = function(){
        this.params = {}
    };

    //파라미터 role 비우기
    _formBuilder.prototype.clearRoles = function(){
        this.validation.roles = {}
        this.validation.messages = {}
    };

    //파라미터 존재 여부
    _formBuilder.prototype.isEmpty = function(){
        return _isEmpty(this.params);
    };

    // INPUT DOM 객체 데이터 저장
    function _inputParamContainer($form) {

        var params = {};
        var roles = {};

        //INPUT 추출
        var _inputParamsExtraction = {
            default: function (inputName, $dom) {
                params[inputName] = $dom.val();
            },
            hidden: function (inputName, $dom) {
                return _inputParamsExtraction.default(inputName, $dom)
            },
            text: function (inputName, $dom) {
                return _inputParamsExtraction.default(inputName, $dom)
            },
            file: function (inputName, $dom) {
                return _inputParamsExtraction.default(inputName, $dom)
            },
            radio: function (inputName, $dom) {
                var value = params[inputName] || '';
                if ($dom.prop('checked')) {
                    value = $dom.val();
                }
                params[inputName] = value;
            },
            checkbox: function (inputName, $dom) {
                if(!params.hasOwnProperty(inputName)){
                    params[inputName] = [];
                }
                if ($dom.prop('checked')) {
                    params[inputName].push($dom.val());
                }
            },
            select: function (inputName, $dom) {
                var value = params[inputName] || '';
                if ($dom.prop('selected')) {
                    value = $dom.val();
                }
                params[inputName] = value;
            },
            selectModule: function (inputName, inputValue) {
                params[inputName] = inputValue;
            }
        };

        //INPUT ROLES 추출
        function _inputRolesExtraction(inputName, $this){
            var inputRole = $this.data(builderRoleHtmlAttr);

            if(!_isEmpty(inputRole)){
                roles[inputName] = inputRole.indexOf('|') !== -1 ? inputRole.split('|') : [inputRole];
            }
        }

        //기본 INPUT
        $form.find('input').each(function(i, data){
            var $this = $(this);

            var inputName = $this.attr('name');
            var inputType = $this.attr('type');
            if(!_isEmpty(inputName) && _inputParamsExtraction.hasOwnProperty(inputType)){
                _inputParamsExtraction[inputType](inputName, $this);
                _inputRolesExtraction(inputName, $this);
            }
        });

        $form.find('textarea').each(function(i, data){
            var $this = $(this);
            var inputName = $this.attr('name');
            _inputParamsExtraction['text'](inputName, $this);
            _inputRolesExtraction(inputName, $this);
        });

        //STOVE 마크업 모듈
        $form.find('.select').each(function(i, data){
            var $this = $(this);
            var inputName = $this.attr('name');
            var inputKey = $this.find('input[type="hidden"].form-key').val();
            var inputMetaName = $this.find('input[type="hidden"].form-value').val();

            if(!_isEmpty(inputName)){
                _inputParamsExtraction['selectModule'](inputName, inputKey);
                _inputRolesExtraction(inputName, $this);
            }
        });

        return {
            params: params,
            validation: { roles: roles }
        }
    }






    $.fn.formBuilder = function(){
        var $form = $(this);
        var inputParamContainer = _inputParamContainer($form);
        return new _formBuilder(inputParamContainer.params).setValidation(inputParamContainer.validation)
    };

    $.fn.formBuilderHTML = function(formBuilder, options){
        var $this = $(this);

        if(!(formBuilder instanceof _formBuilder) && !_isObject(formBuilder) ){
            throw new Error(UNDEFINED_FORM_BUILDER_ERROR);
        }

        if(!(formBuilder instanceof _formBuilder) && _isObject(formBuilder)){
            formBuilder = new _formBuilder(formBuilder)
        }
        formBuilder.$html($this).htmlUpdate();
    };


    var formBuilderUt = {
        create: function (params, roles) {
            var formBuilder = new _formBuilder(params);
            if(roles){
                formBuilder.setValidation(roles)
            }
            return formBuilder;
        }
    };
    return formBuilderUt;

})(jQuery);